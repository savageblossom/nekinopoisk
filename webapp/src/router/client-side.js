export default [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/ClientSide/Home"),
  },
  {
    path: "/movies",
    name: "Movies",
    component: () => import("@/views/ClientSide/Movies"),
  },
  {
    path: "/movie/:id",
    name: "MovieDetails",
    component: () => import("@/views/ClientSide/MovieDetails"),
  },
  {
    path: "/actors",
    name: "Actors",
    component: () => import("@/views/ClientSide/Actors"),
  },
  {
    path: "/actors/:id",
    name: "ActorDetails",
    component: () => import("@/views/ClientSide/ActorDetails"),
  },
];
