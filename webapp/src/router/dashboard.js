export default [
  {
    path: "/admin",
    redirect: { name: "AdminUsers" },
  },
  // MOVIES
  {
    path: "/admin/movies",
    name: "AdminMovies",
    component: () => import("@/views/Admin/AdminMovies"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/movies/new",
    name: "AdminMoviesCreate",
    component: () => import("@/views/Admin/AdminMoviesCreateUpdate"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/movies/edit/:id",
    name: "AdminMoviesUpdate",
    component: () => import("@/views/Admin/AdminMoviesCreateUpdate"),
    meta: { requiresAuth: true },
  },
  // ACTORS
  {
    path: "/admin/actors",
    name: "AdminActors",
    component: () => import("@/views/Admin/AdminCastPersons"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/actors/new",
    name: "AdminCastPersonsCreate",
    component: () => import("@/views/Admin/AdminCastPersonsCreateUpdate"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/actors/edit/:id",
    name: "AdminCastPersonsUpdate",
    component: () => import("@/views/Admin/AdminCastPersonsCreateUpdate"),
    meta: { requiresAuth: true },
  },
  // USERS
  {
    path: "/admin/users",
    name: "AdminUsers",
    component: () => import("@/views/Admin/AdminUsers"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/users/new",
    name: "AdminUsersCreate",
    component: () => import("@/views/Admin/AdminUsersCreateUpdate"),
    meta: { requiresAuth: true },
  },
  {
    path: "/admin/users/edit/:id",
    name: "AdminUsersUpdate",
    component: () => import("@/views/Admin/AdminUsersCreateUpdate"),
    meta: { requiresAuth: true },
  },
  // ADMIN AUTH
  {
    path: "/login",
    name: "AdminAuth",
    component: () => import("@/views/Admin/AdminAuth"),
  },
];
