import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

import clientSideRoutes from "./client-side";
import dashboardRoutes from "./dashboard";

Vue.use(VueRouter);

const routes = [...clientSideRoutes, ...dashboardRoutes];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach(async (to, from, next) => {
  await store.dispatch("verify_token");
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const is_admin_logged = store.getters.is_admin_logged;
    if (!is_admin_logged) {
      next({
        path: "/admin/login",
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
