import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";
import setupAxios from "./axios";
import VueUploadComponent from "vue-upload-component";
import VueCarousel from "vue-carousel";

setupAxios();

Vue.config.productionTip = false;

Vue.use(VueMaterial);
Vue.use(VueAxios, axios);
Vue.use(VueCarousel);

Vue.component("file-upload", VueUploadComponent);

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
