import Vue from "vue";
import VueAxios from "vue-axios";
import axios from "axios";
import store from "./store";

export default function () {
  Vue.use(VueAxios, axios);

  axios.interceptors.request.use(
    (request) => {
      const token = store.getters.token;
      if (token) {
        request.headers.Authorization = `Bearer ${token}`;
      }
      return request;
    },
    (err) => {
      return Promise.reject(err);
    }
  );
}
