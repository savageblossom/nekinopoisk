import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const baseUrl = process.env.VUE_APP_BASE_URL;

export default new Vuex.Store({
  state: {
    users: [],
    user: {
      name: "",
      email: "",
      favoriteCastPersonsIds: null,
      favoriteMoviesIds: null,
      reviewsIds: null,
      role: "",
      password: "",
    },
    // MOVIES
    movies: [],
    movie: {
      title: "",
      description: "",
      year: new Date(),
      rating: 0,
      castPersons: [],
    },
    // CAST-PERSONS
    castPerson: [],
    castPersons: {},
    // AUTH
    is_admin_logged: false,
    logged_user: null,
    token: "",
  },
  mutations: {
    set_users(state, data) {
      state.users = data;
    },
    set_user(state, data) {
      state.user = data;
    },
    // MOVIES
    set_movies(state, data) {
      state.movies = data;
    },
    set_movie(state, data) {
      state.movie = data;
    },
    // MOVIES
    set_cast_persons(state, data) {
      state.castPersons = data;
    },
    set_cast_person(state, data) {
      state.castPerson = data;
    },
    // AUTH
    set_admin_logged(state, truthy) {
      state.is_admin_logged = truthy;
    },
    set_logged_user(state, user) {
      state.logged_user = user;
    },
    set_token(state, token) {
      state.token = token;
    },
  },
  actions: {
    // USERS
    async fetch_users({ commit }, params) {
      const response = await Vue.$http.get(`${baseUrl}/user`, { params });

      if ("data" in response) {
        commit("set_users", response.data);
      }
    },
    async fetch_user({ commit }, { _id }) {
      const user = await Vue.$http.get(`${baseUrl}/user/${_id}`);
      if ("data" in user) {
        commit("set_user", user.data);
      }
    },
    async update_user(_, { _id, body }) {
      await Vue.$http.put(`${baseUrl}/user/${_id}`, body);
    },
    async create_user(_, { body }) {
      await Vue.$http.post(`${baseUrl}/user`, body);
    },
    async delete_user(_, { _id }) {
      await Vue.$http.delete(`${baseUrl}/user/${_id}`);
    },
    // CAST-PERSONS
    async fetch_cast_persons({ commit }, params) {
      const response = await Vue.$http.get(`${baseUrl}/cast-person`, {
        params,
      });

      if ("data" in response) {
        commit("set_cast_persons", response.data);
      }
    },
    async fetch_cast_person({ commit }, { _id }) {
      const response = await Vue.$http.get(`${baseUrl}/cast-person/${_id}`);
      if ("data" in response) {
        commit("set_cast_person", response.data);
      }
    },
    async update_cast_person(_, { _id, body }) {
      await Vue.$http.put(`${baseUrl}/cast-person/${_id}`, body);
    },
    async create_cast_person(_, { body }) {
      await Vue.$http.post(`${baseUrl}/cast-person`, body);
    },
    async delete_cast_person(_, { _id }) {
      await Vue.$http.delete(`${baseUrl}/cast-person/${_id}`);
    },
    //MOVIES
    async fetch_movies({ commit }, config = { params: {}, returnOnly: false }) {
      const { params, returnOnly } = config;
      console.log(params);
      const response = await Vue.$http.get(`${baseUrl}/movie`, { params });

      if ("data" in response) {
        return returnOnly ? response.data : commit("set_movies", response.data);
      }
    },
    async fetch_movie({ commit }, { _id }) {
      const response = await Vue.$http.get(`${baseUrl}/movie/${_id}`);
      if ("data" in response) {
        commit("set_movie", response.data);
      }
    },
    async update_movie(_, { _id, body }) {
      await Vue.$http.put(`${baseUrl}/movie/${_id}`, body);
    },
    async create_movie(_, { body }) {
      await Vue.$http.post(`${baseUrl}/movie`, body);
    },
    async delete_movie(_, { _id }) {
      await Vue.$http.delete(`${baseUrl}/movie/${_id}`);
    },
    // AUTH
    async verify_token({ commit, state }) {
      const access_token = localStorage.getItem("token") || state.token;
      const logged_user = JSON.parse(localStorage.getItem("user"));

      try {
        await Vue.$http.post(`${baseUrl}/auth/verify`, {
          access_token,
        });
        commit("set_admin_logged", true);
        commit("set_logged_user", logged_user);
        commit("set_token", access_token);
        return;
      } catch (_) {
        commit("set_admin_logged", false);
        commit("set_logged_user", null);
        commit("set_token", "");
        localStorage.removeItem("token");
        localStorage.removeItem("user");

        return;
      }
    },
  },
  getters: {
    // USERS
    users(state) {
      return state.users;
    },
    user(state) {
      return state.user;
    },
    // MOVIES
    movies(state) {
      return state.movies;
    },
    movie(state) {
      return state.movie;
    },
    // MOVIES
    cast_person(state) {
      return state.castPerson;
    },
    cast_persons(state) {
      return state.castPersons;
    },
    // AUTH
    is_admin_logged(state) {
      return state.is_admin_logged;
    },
    token(state) {
      return state.token;
    },
    logged_user(state) {
      return state.logged_user;
    },
  },
});
