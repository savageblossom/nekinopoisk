import { Types } from 'mongoose';

const transformQuery = (query) => {
  const filterList = {
    in({ trimmedValue }) {
      return {
        $in: trimmedValue,
      };
    },
    contains({ trimmedValue }) {
      const untrimmedValue = trimmedValue.join(',');
      return {
        $regex: untrimmedValue,
        $options: 'i',
      };
    },
  };

  Object.keys(query).forEach((key) => {
    const trimmedKey = key.split('.')[0];
    const aggregationKey = key.split('.')[1];
    let trimmedValue = query[key].split(',');

    if (trimmedKey === '_id') {
      trimmedValue = trimmedValue.map((queryParam) =>
        Types.ObjectId(queryParam),
      );
    }

    if (!(aggregationKey in filterList)) {
      return;
    }

    query[trimmedKey] = filterList[aggregationKey]({ trimmedValue });

    delete query[key];
  });

  return query;
};

export default transformQuery;
