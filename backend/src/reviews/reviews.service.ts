import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { Review, ReviewDocument } from './schemas/review.schema';

const applyReviewsAggregationQuery = (query) => {
  return [
    {
      $match: query,
    },
    {
      $lookup: {
        from: 'movies',
        localField: 'movieId',
        foreignField: '_id',
        as: 'movie',
      },
    },
    {
      $unwind: '$movie',
    },
    {
      $lookup: {
        from: 'users',
        localField: 'authorId',
        foreignField: '_id',
        as: 'author',
      },
    },
    {
      $unwind: '$author',
    },
  ];
};

@Injectable()
export class ReviewsService {
  constructor(
    @InjectModel(Review.name) private reviewModel: Model<ReviewDocument>,
  ) {}

  async findOne(id: string): Promise<Review> {
    return this.reviewModel.findById(id);
  }

  async find(query): Promise<Review[]> {
    const aggregationQuery = applyReviewsAggregationQuery(query);

    return this.reviewModel.aggregate(aggregationQuery);
  }

  async create(movieDto: CreateReviewDto): Promise<Review> {
    const newReview = new this.reviewModel(movieDto);

    return newReview.save();
  }

  async delete(id: string): Promise<Review> {
    return this.reviewModel.findByIdAndRemove(id);
  }

  async update(id: string, movieDto: UpdateReviewDto): Promise<Review> {
    return this.reviewModel.findByIdAndUpdate(id, movieDto, { new: true });
  }
}
