import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { Movie } from 'src/movies/schemas/movie.schema';
import { User } from 'src/users/schemas/user.schema';

export type ReviewDocument = Review & Document;

@Schema()
export class Review {
  @Prop()
  title: string;

  @Prop()
  reviewText: string;

  @Prop()
  authorId: string;

  @Prop()
  movieRating: number;

  @Prop({ type: SchemaTypes.ObjectId, ref: Movie, required: true })
  movieId: string;
}

export const ReviewSchema = SchemaFactory.createForClass(Review);
