export class CreateReviewDto {
  readonly title: string;
  readonly reviewText: string;
  readonly author: string;
  readonly movieRating: number;
  readonly movieId: string;
}
