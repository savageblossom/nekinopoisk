export class UpdateReviewDto {
  readonly title: string;
  readonly reviewText: string;
  readonly author: string;
  readonly movieRating: number;
  readonly movieId: string;
}
