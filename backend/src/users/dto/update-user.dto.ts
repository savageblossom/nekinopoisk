export class UpdateUserDto {
  readonly name: string;
  readonly email: string;
  readonly reviewsIds: string[];
  readonly favoriteMoviesIds: string[];
  readonly favoriteCastPersonsIds: string[];
  readonly role: string;
  readonly createDate: Date;
  readonly avatar: string;
}
