import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { CastPerson } from 'src/cast-persons/schemas/cast-person';
import { Movie } from 'src/movies/schemas/movie.schema';
import { Review } from 'src/reviews/schemas/review.schema';
// import { Review } from 'src/reviews/schemas/review.schema';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  email: string;

  @Prop([{ type: SchemaTypes.ObjectId, ref: Review }])
  reviewsIds: string[];

  @Prop([{ type: SchemaTypes.ObjectId, ref: Movie }])
  favoriteMoviesIds: string[];

  @Prop([{ type: SchemaTypes.ObjectId, ref: CastPerson }])
  favoriteCastPersonsIds: string[];

  @Prop({ required: true })
  role: string;

  @Prop()
  avatar: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
