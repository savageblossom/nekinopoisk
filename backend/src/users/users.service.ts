import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schemas/user.schema';

const applyUsersAggregationQuery = (query) => {
  return [
    {
      $match: query,
    },
    {
      $lookup: {
        from: 'castpeople',
        localField: 'favoriteCastPersonsIds',
        foreignField: '_id',
        as: 'favoriteCastPersons',
      },
    },
    {
      $lookup: {
        from: 'movies',
        localField: 'favoriteMoviesIds',
        foreignField: '_id',
        as: 'favoriteMovies',
      },
    },
  ];
};
@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findOne(id: string): Promise<any> {
    const query = {
      _id: Types.ObjectId(id),
    };

    const aggregationQuery = applyUsersAggregationQuery(query);

    const queryResult = await this.userModel.aggregate(aggregationQuery);

    return queryResult.length ? queryResult[0] : {};
  }

  async find(query): Promise<User[]> {
    const aggregationQuery = applyUsersAggregationQuery(query);

    return this.userModel.aggregate(aggregationQuery);
  }

  async create(userDto: CreateUserDto): Promise<User> {
    const newUser = new this.userModel(userDto);

    return newUser.save();
  }

  async delete(id: string): Promise<User> {
    return this.userModel.findByIdAndRemove(id);
  }

  async update(id: string, userDto: UpdateUserDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(id, userDto, { new: true });
  }
}
