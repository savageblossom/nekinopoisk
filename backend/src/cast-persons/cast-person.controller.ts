import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CastPersonService } from './cast-persons.service';
import { CreateCastPersonDto } from './dto/create-cast-person.dto';
import { UpdateCastPersonDto } from './dto/update-cast-person.dto';
import { CastPerson } from './schemas/cast-person';

@Controller('cast-person')
export class CastPersonController {
  constructor(private readonly castPersonService: CastPersonService) {}

  @Get()
  getAll(@Query() query): Promise<CastPerson[]> {
    return this.castPersonService.find(query);
  }

  @Post()
  create(
    @Body() createCastPersonDto: CreateCastPersonDto,
  ): Promise<CastPerson> {
    return this.castPersonService.create(createCastPersonDto);
  }

  @Get(':id')
  getOne(@Param('id') id: string): Promise<CastPerson> {
    return this.castPersonService.findOne(id);
  }

  // @UseGuards(JwtAuthGuard)
  @Delete(':id')
  delete(@Param('id') id: string): Promise<CastPerson> {
    return this.castPersonService.delete(id);
  }

  // @UseGuards(JwtAuthGuard)
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateCastPersoDton: UpdateCastPersonDto,
  ): Promise<CastPerson> {
    return this.castPersonService.update(id, updateCastPersoDton);
  }
}
