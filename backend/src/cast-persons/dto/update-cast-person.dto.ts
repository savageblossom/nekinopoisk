export class UpdateCastPersonDto {
  readonly name: string;
  readonly birthDate: Date;
  readonly occupation: string;
  readonly moviesStarredIds: string[];
  readonly previews: string[];
}
