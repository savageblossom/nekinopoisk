export class CreateCastPersonDto {
  readonly name: string;
  readonly birthDate: Date;
  readonly occupation: string;
  readonly moviesStarredIds: string[];
  readonly previews: string[];
}
