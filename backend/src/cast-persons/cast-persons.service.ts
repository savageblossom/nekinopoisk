import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import transformQuery from 'src/helpers/queryTransform';
import { CreateCastPersonDto } from './dto/create-cast-person.dto';
import { UpdateCastPersonDto } from './dto/update-cast-person.dto';
import { CastPerson, CastPersonDocument } from './schemas/cast-person';

const applyCastPersonsAggregationQuery = (query) => {
  return [
    {
      $match: query,
    },
    {
      $lookup: {
        from: 'movies',
        localField: 'moviesStarredIds',
        foreignField: '_id',
        as: 'moviesStarred',
      },
    },
  ];
};
@Injectable()
export class CastPersonService {
  constructor(
    @InjectModel(CastPerson.name)
    private castPersonModel: Model<CastPersonDocument>,
  ) {}

  async findOne(id: string): Promise<any> {
    const query = {
      _id: Types.ObjectId(id),
    };

    const aggregationQuery = applyCastPersonsAggregationQuery(query);

    const queryResult = await this.castPersonModel.aggregate(aggregationQuery);

    return queryResult.length ? queryResult[0] : {};
  }

  async find(query): Promise<CastPerson[]> {
    return this.castPersonModel.find(transformQuery(query));
  }

  async create(userDto: CreateCastPersonDto): Promise<CastPerson> {
    const newUser = new this.castPersonModel(userDto);

    return newUser.save();
  }

  async delete(id: string): Promise<CastPerson> {
    return this.castPersonModel.findByIdAndRemove(id);
  }

  async update(id: string, userDto: UpdateCastPersonDto): Promise<CastPerson> {
    return this.castPersonModel.findByIdAndUpdate(id, userDto, { new: true });
  }
}
