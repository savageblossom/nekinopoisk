import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CastPersonController } from './cast-person.controller';
import { CastPersonService } from './cast-persons.service';
import { CastPerson, CastPersonSchema } from './schemas/cast-person';

@Module({
  providers: [CastPersonService],
  controllers: [CastPersonController],
  imports: [
    MongooseModule.forFeature([
      { name: CastPerson.name, schema: CastPersonSchema },
    ]),
  ],
})
export class CastPersonModule {}
