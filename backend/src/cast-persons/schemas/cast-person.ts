import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { Movie } from 'src/movies/schemas/movie.schema';

export type CastPersonDocument = CastPerson & Document;

@Schema()
export class CastPerson {
  @Prop({ required: true })
  name: string;

  @Prop()
  birthDate: Date;

  @Prop({ required: true })
  occupation: string;

  @Prop([{ type: SchemaTypes.ObjectId, ref: Movie }])
  moviesStarredIds: string[];

  @Prop()
  previews: string[];
}

export const CastPersonSchema = SchemaFactory.createForClass(CastPerson);
