import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import transformQuery from 'src/helpers/queryTransform';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Movie, MovieDocument } from './schemas/movie.schema';

const applyMoviesAggregationQuery = (query) => {
  return [
    {
      $match: query,
    },
    {
      $lookup: {
        from: 'castpeople',
        localField: 'castPersonsIds',
        foreignField: '_id',
        as: 'castPersons',
      },
    },
  ];
};

@Injectable()
export class MoviesService {
  constructor(
    @InjectModel(Movie.name) private movieModel: Model<MovieDocument>,
  ) {}

  async findOne(id: string): Promise<Movie> {
    const query = {
      _id: Types.ObjectId(id),
    };

    const aggregationQuery = applyMoviesAggregationQuery(query);

    const queryResult = await this.movieModel.aggregate(aggregationQuery);

    return queryResult.length ? queryResult[0] : {};
  }

  async find(query): Promise<Movie[]> {
    const aggregationQuery = applyMoviesAggregationQuery(transformQuery(query));

    return this.movieModel.aggregate(aggregationQuery);
  }

  async create(movieDto: CreateMovieDto): Promise<Movie> {
    const newUser = new this.movieModel(movieDto);

    return newUser.save();
  }

  async delete(id: string): Promise<Movie> {
    return this.movieModel.findByIdAndRemove(id);
  }

  async update(id: string, movieDto: UpdateMovieDto): Promise<Movie> {
    return this.movieModel.findByIdAndUpdate(id, movieDto, { new: true });
  }
}
