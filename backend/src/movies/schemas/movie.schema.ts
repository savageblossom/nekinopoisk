import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { CastPerson } from 'src/cast-persons/schemas/cast-person';

export type MovieDocument = Movie & Document;

@Schema()
export class Movie {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  year: Date;

  @Prop()
  previews: string[];

  @Prop()
  genre: string;

  @Prop()
  rating: number;

  @Prop()
  reviewsIds: string[];

  @Prop([{ type: SchemaTypes.ObjectId, ref: CastPerson }])
  castPersonsIds: string[];
}

export const MovieSchema = SchemaFactory.createForClass(Movie);
