export class UpdateMovieDto {
  readonly title: string;
  readonly description: string;
  readonly year: Date;
  readonly previews: string[];
  readonly genre: string;
  readonly rating: number;
  readonly reviewsIds: string[];
  readonly castPersonsIds: string[];
}
