import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CastPersonModule } from './cast-persons/cast-person.module';
import { MoviesModule } from './movies/movies.module';
import { ReviewsModule } from './reviews/reviews.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    UsersModule,
    CastPersonModule,
    MoviesModule,
    ReviewsModule,
    MongooseModule.forRoot(
      `mongodb+srv://umer:OP5VTjmmYwRoL8hf@cluster0.ymqno.mongodb.net/nekinopoisk?retryWrites=true&w=majority`,
    ),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
